﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list1 = GetStudents(70).ToList();
            List<Student> list2 = GetStudents(0).ToList();
            var res1 = list1.Any();
            var res2 = list2.Any();
            var res3 = list1.GroupBy(p => p.Grade).ToList();

            Console.ReadKey();
        }
        static IEnumerable<Student> GetStudents (int count)
        {
            var rnd = new Random();
            for (int i = 1; i <= count; i++)
            {
                var student = new Student
                {
                    Name = $"A{i}",
                    Surname = $"A{i}yan",
                    Age = rnd.Next(15,30),
                    Grade = (byte)rnd.Next(0, 101)
                };
                yield return student;
            }
        }
    }
}
